package com.ptit.trongphu.tourist.views.destinations.description;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.airbnb.lottie.LottieAnimationView;
import com.ptit.trongphu.tourist.R;
import com.ptit.trongphu.tourist.common.adapter.RestaurantsCardViewAdapter;
import com.ptit.trongphu.tourist.common.base.BaseActivity;
import com.ptit.trongphu.tourist.common.model.City;
import com.ptit.trongphu.tourist.common.model.RestaurantItemEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.ptit.trongphu.tourist.utils.Constants.BASE_URL;
import static com.ptit.trongphu.tourist.utils.Constants.AUTHORIZATION;
import static com.ptit.trongphu.tourist.utils.Constants.EXTRA_MESSAGE_CITY_OBJECT;
import static com.ptit.trongphu.tourist.utils.Constants.USER_TOKEN;


public class RestaurantsActivity extends BaseActivity implements RestaurantsCardViewAdapter.OnItemClickListener,View.OnClickListener {

    private static final String TAG = "RestaurantsActivity";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.restaurants_recycler_view)
    RecyclerView mRestaurantsOptionsRecycleView;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;

    private City mCity;
    private Handler mHandler;
    private String mToken;
    public List<RestaurantItemEntity> restaurantItemEntities = new ArrayList<>();
    private RestaurantsCardViewAdapter mRestaurantsCardViewAdapter;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, RestaurantsActivity.class);
    }

    @Override
    protected int getLayoutResources() {
        return R.layout.activity_restaurants;
    }

    @Override
    protected void initVariables(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        getData();
        setToolbar(toolbar, "Restaurants " + mCity.getNickname());

        mHandler = new Handler(Looper.getMainLooper());
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mToken = mSharedPreferences.getString(USER_TOKEN, null);

        mRestaurantsCardViewAdapter = new RestaurantsCardViewAdapter(this,
                restaurantItemEntities);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(RestaurantsActivity.this);
        mRestaurantsOptionsRecycleView.setLayoutManager(mLayoutManager);
        mRestaurantsOptionsRecycleView.setItemAnimator(new DefaultItemAnimator());
        mRestaurantsOptionsRecycleView.setAdapter(mRestaurantsCardViewAdapter);
    }


    private void getData() {
        Intent intent = getIntent();
        mCity = (City) intent.getSerializableExtra(EXTRA_MESSAGE_CITY_OBJECT);
    }

    private void setToolbar(Toolbar toolbar, String tittle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSupportActionBar(toolbar);
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(tittle);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_back_white);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getRestaurantItems();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!restaurantItemEntities.isEmpty())
            getMenuInflater().inflate(R.menu.restaurants_sort_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                sortRestaurants(item.getItemId());
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void sortRestaurants(int sortType) {
        Comparator<RestaurantItemEntity> comparator = null;
        switch (sortType) {
            case R.id.restaurantSortPricesLow:
                comparator = (r1, r2) -> r1.getAvgCost() - r2.getAvgCost();
                break;
            case R.id.restaurantSortPricesHigh:
                comparator = (r1, r2) -> r2.getAvgCost() - r1.getAvgCost();
                break;
            case R.id.restaurantSortRating:
                comparator = (r1, r2) -> Float.compare(r2.getRatings(), r1.getRatings());
                break;
            case R.id.restaurantSortVotes:
                comparator = (r1, r2) -> r2.getVotes() - r1.getVotes();
                break;
            case R.id.restaurantSortAlphabet:
                comparator = (r1, r2) -> r1.getName().compareTo(r2.getName());
                break;
        }

        if (comparator != null) {
            Collections.sort(restaurantItemEntities, comparator);
            if (mRestaurantsCardViewAdapter != null) {
                mRestaurantsCardViewAdapter.notifyDataSetChanged();
            }
        }
    }


    @Override
    public void onItemClick(RestaurantItemEntity item) {
        Uri url = Uri.parse(item.getURL());
        Intent intent = new Intent(Intent.ACTION_VIEW, url);
        startActivity(intent);
    }

    private void getRestaurantItems() {

        String uri = BASE_URL + "get-all-restaurants/" + mCity.getLatitude() + "/" + mCity.getLongitude();
        Log.v(TAG, "URI : " + uri);

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .header(AUTHORIZATION, "Token " + mToken)
                .url(uri)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                mHandler.post(() -> networkError());
                Log.v(TAG, "Request Failed, message = " + e.getMessage());
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String res = Objects.requireNonNull(response.body()).string();

                mHandler.post(() -> {

                    if (res.equals("\"Not Found\"") || res.contains("Not Found")) {
                        notFoundError();
                        return;
                    }

                    try {
                        JSONArray array = new JSONArray(res);
                        Log.v(TAG, "Response = " + res);
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            String id = object.getString("restaurant_id");
                            String imageUrl = object.getString("restaurant_image");
                            String name = object.getString("restaurant_name");
                            String address = object.getString("address");
                            float ratings = (float) object.getDouble("aggregate_rating");
                            int votes = object.getInt("votes");
                            String restaurantURL = object.getString("restaurant_url");
                            int avgCost = object.getInt("avg_cost_2");

                            restaurantItemEntities.add(
                                    new RestaurantItemEntity(id, imageUrl, name, address, ratings,
                                            votes, avgCost, restaurantURL));
                        }

                        animationView.setVisibility(View.GONE);
                        mRestaurantsCardViewAdapter.notifyDataSetChanged();

                        invalidateOptionsMenu();
                    } catch (JSONException e) {
                        networkError();
                        e.printStackTrace();
                        Log.e("ERROR : ", "Message : " + e.getMessage());
                    }
                });

            }
        });
    }

    private void notFoundError() {
        animationView.setAnimation(R.raw.empty_list);
        animationView.playAnimation();
        animationView.setOnClickListener(v -> getRestaurantItems());
    }


    private void networkError() {
        animationView.setAnimation(R.raw.network_lost);
        animationView.playAnimation();
        animationView.setOnClickListener(v -> getRestaurantItems());
    }

    @Override
    public void onClick(View v) {

    }
}