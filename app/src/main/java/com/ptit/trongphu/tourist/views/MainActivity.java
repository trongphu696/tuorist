package com.ptit.trongphu.tourist.views;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ptit.trongphu.tourist.R;
import com.ptit.trongphu.tourist.common.base.BaseActivity;
import com.ptit.trongphu.tourist.views.destinations.CityFragment;
import com.ptit.trongphu.tourist.views.login.LoginActivity;
import com.ptit.trongphu.tourist.views.mytrips.MyTripsFragment;
import com.ptit.trongphu.tourist.views.tourist.TravelFragment;
import com.ptit.trongphu.tourist.views.utilities.AboutUsFragment;
import com.ptit.trongphu.tourist.views.utilities.UtilitiesFragment;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.ptit.trongphu.tourist.utils.Constants.BASE_URL;
import static com.ptit.trongphu.tourist.utils.Constants.AUTHORIZATION;
import static com.ptit.trongphu.tourist.utils.Constants.USER_DATE_JOINED;
import static com.ptit.trongphu.tourist.utils.Constants.USER_EMAIL;
import static com.ptit.trongphu.tourist.utils.Constants.USER_ID;
import static com.ptit.trongphu.tourist.utils.Constants.USER_IMAGE;
import static com.ptit.trongphu.tourist.utils.Constants.USER_NAME;
import static com.ptit.trongphu.tourist.utils.Constants.USER_STATUS;
import static com.ptit.trongphu.tourist.utils.Constants.USER_TOKEN;
import static com.ptit.trongphu.tourist.utils.DateUtils.getDate;
import static com.ptit.trongphu.tourist.utils.DateUtils.rfc3339ToMills;


public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawer;

    private ActionBarDrawerToggle toggle;
    private static final String TAG = "MainActivity";

    private SharedPreferences mSharedPreferences;
    private String mToken;
    private Handler mHandler;
    private int mPreviousMenuItemId;

    @Override
    protected int getLayoutResources() {
        return R.layout.activity_main;
    }

    @Override
    protected void initVariables(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        setSupportActionBar(toolbar);
        setTitle("Home");
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mToken = mSharedPreferences.getString(USER_TOKEN, null);
        mHandler = new Handler(Looper.getMainLooper());


        // Get runtime permissions for Android M
        getRuntimePermissions();

        mDrawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this,
                mDrawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();

        String emailId = mSharedPreferences.getString(USER_EMAIL, getString(R.string.app_name));
        fillNavigationView(emailId, null);
        initFragmentBegin();
        getProfileInfo();

    }

    private void setToolbar(Toolbar toolbar, String tittle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSupportActionBar(toolbar);
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(tittle);
        }

    }

    private void initFragmentBegin() {
        Fragment fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragment = CityFragment.newInstance();
        defaultSelectedNavMenu(R.id.nav_city);
        mPreviousMenuItemId = R.id.nav_city;

        fragmentManager.beginTransaction().replace(R.id.inc, fragment).commit();
    }

    void defaultSelectedNavMenu(int resId) {
        NavigationView navigationView = findViewById(R.id.nav_view);
        Menu menu = navigationView.getMenu();
        MenuItem menuItem = menu.findItem(resId);
        menuItem.setChecked(true);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == mPreviousMenuItemId) {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }

        int id = item.getItemId();
        mPreviousMenuItemId = item.getItemId();
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = getFragmentByNavMenuItemId(id);
        if (fragment != null) {
            fragmentManager.beginTransaction().replace(R.id.inc, fragment).commit();
        }

        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private Fragment getFragmentByNavMenuItemId(int id) {
        Fragment fragment = null;
        switch (id) {
            case R.id.nav_travel:
                fragment = TravelFragment.newInstance();
                setTitle(getString(R.string.my_trip));
                break;

            case R.id.nav_mytrips:
                fragment = MyTripsFragment.newInstance();
                setTitle(getString(R.string.my_trip));
                break;

            case R.id.nav_city:
                fragment = CityFragment.newInstance();
                break;

            case R.id.nav_utility:
                fragment = UtilitiesFragment.newInstance();
                setTitle(getString(R.string.navigation_drawer_utilities));
                break;

            case R.id.nav_about_us:
                fragment = AboutUsFragment.newInstance();
                setTitle(getString(R.string.navigation_drawer_about_us));
                break;

            case R.id.nav_signout: {

                ContextThemeWrapper crt = new ContextThemeWrapper(this, R.style.AlertDialog);
                AlertDialog.Builder builder = new AlertDialog.Builder(crt);
                builder.setMessage(R.string.signout_message)
                        .setPositiveButton(R.string.positive_button,
                                (dialog, which) -> {
                                    mSharedPreferences
                                            .edit()
                                            .putString(USER_TOKEN, null)
                                            .apply();
                                    Intent i = LoginActivity.getStartIntent(MainActivity.this);
                                    startActivity(i);
                                    finish();
                                })
                        .setNegativeButton(android.R.string.cancel,
                                (dialog, which) -> {

                                });
                builder.create().show();
                break;
            }
            case R.id.nav_settings:
                fragment = SettingsFragment.newInstance();
                setTitle(getString(R.string.navigation_drawer_settings));
                break;
        }
        return fragment;
    }

    private void getRuntimePermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(MainActivity.this,
                    Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.BLUETOOTH,
                        Manifest.permission.BLUETOOTH_ADMIN,
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.WRITE_CONTACTS,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.WAKE_LOCK,
                        Manifest.permission.INTERNET,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.VIBRATE,
                }, 0);
            }
        }
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    private void fillNavigationView(String emailId, String imageURL) {

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View navigationHeader = navigationView.getHeaderView(0);
        TextView emailTextView = navigationHeader.findViewById(R.id.email);
        emailTextView.setText(emailId);

        // click to view profile
        ImageView imageView = navigationHeader.findViewById(R.id.image);
        Picasso.with(MainActivity.this).load(imageURL).placeholder(R.drawable.icon_profile)
                .error(R.drawable.icon_profile).into(imageView);
        imageView.setOnClickListener(v -> startActivity(ProfileActivity.getStartIntent(MainActivity.this)));
    }

    private void getProfileInfo() {

        String uri = BASE_URL + "get-user";
        Log.v(TAG, "url=" + uri);

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .header(AUTHORIZATION, "Token " + mToken)
                .url(uri)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "request Failed, message = " + e.getMessage());
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String res = Objects.requireNonNull(response.body()).string();

                mHandler.post(() -> {
                    try {
                        JSONObject object = new JSONObject(res);
                        String userName = object.getString("username");
                        String firstName = object.getString("first_name");
                        String lastName = object.getString("last_name");
                        String status = object.getString("status");
                        int id = object.getInt("id");
                        String imageURL = object.getString("image");
                        String dateJoined = object.getString("date_joined");
                        Long dateTime = rfc3339ToMills(dateJoined);
                        String date = getDate(dateTime);

                        String fullName = firstName + " " + lastName;
                        mSharedPreferences.edit().putString(USER_NAME, fullName).apply();
                        mSharedPreferences.edit().putString(USER_EMAIL, userName).apply();
                        mSharedPreferences.edit().putString(USER_DATE_JOINED, date).apply();
                        mSharedPreferences.edit().putString(USER_IMAGE, imageURL).apply();
                        mSharedPreferences.edit().putString(USER_STATUS, status).apply();
                        mSharedPreferences.edit().putString(USER_ID, String.valueOf(id)).apply();
                        fillNavigationView(fullName, imageURL);

                    } catch (JSONException e) {
                        Log.e(TAG, "Error parsing user JSON, " + e.getMessage());
                    }
                });

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        fillNavigationView(mSharedPreferences.getString(USER_NAME, getString(R.string.app_name)),
                mSharedPreferences.getString(USER_IMAGE, null));
        invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        item.getItemId();
        return toggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }
}