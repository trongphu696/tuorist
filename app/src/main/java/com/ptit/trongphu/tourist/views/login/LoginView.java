package com.ptit.trongphu.tourist.views.login;

interface LoginView {

    void rememberUserInfo(String token, String email);

    void startMainActivity();

    void showError();

    void showLoadingDialog();

    void dismissLoadingDialog();

    void getRunTimePermissions();

    void checkUserSession();

    void openSignUp();

    void openLogin();

    void setLoginEmail(String email);

    void showMessage(String message);

    void forgotPassword();

    void openResetPin(String email);

    void resendResetCode();

    void newPasswordInput();

    void confirmPasswordReset(String email);
}
