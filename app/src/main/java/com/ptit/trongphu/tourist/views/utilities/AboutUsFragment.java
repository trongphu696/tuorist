package com.ptit.trongphu.tourist.views.utilities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.ptit.trongphu.tourist.R;
import com.ptit.trongphu.tourist.common.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ptit.trongphu.tourist.utils.Constants.BITBUCKET;
import static com.ptit.trongphu.tourist.utils.Constants.EMAIL;
import static com.ptit.trongphu.tourist.utils.Constants.PRIVACY_POLICY;
import static com.ptit.trongphu.tourist.utils.Constants.WEBSITE;

public class AboutUsFragment extends BaseFragment {
    @BindView(R.id.version_code)
    TextView txt_version_code;

    private Context mContext;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_about_us;
    }
    @Override
    protected void initVariables(Bundle saveInstanceState, View rootView) {
        ButterKnife.bind(this, rootView);
    }

    @Override
    protected void initData(Bundle saveInstanceState) {
        getActivity().setTitle(getString(R.string.navigation_drawer_about_us));
        try {
            PackageInfo pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            String version = pInfo.versionName;
            txt_version_code.setText(version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static AboutUsFragment newInstance() {
        AboutUsFragment fragment = new AboutUsFragment();
        return fragment;
    }

//    private void setToolbar() {
//        AppCompatActivity activity = (AppCompatActivity) getActivity();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            activity.setSupportActionBar(toolbar);
//        }
//        ActionBar actionBar = activity.getSupportActionBar();
//        if (activity.getSupportActionBar() != null) {
//            actionBar.setDisplayHomeAsUpEnabled(true);
//            actionBar.setTitle(getString(R.string.about_us));
//            actionBar.setIcon(R.drawable.ic_back_whiite);
//        }
//
//    }

    @OnClick(R.id.cv_bitbucket)
    public void onForkClicked() {
        Intent viewIntent =
                new Intent(Intent.ACTION_VIEW, Uri.parse(BITBUCKET));
        startActivity(viewIntent);
    }

    @OnClick(R.id.cv_privacy_policy)
    public void onPrivacyPolicyClicked() {
        Intent viewIntent =
                new Intent(Intent.ACTION_VIEW, Uri.parse(PRIVACY_POLICY));
        startActivity(viewIntent);
    }

    @OnClick(R.id.cv_website)
    public void onWebsiteClicked() {
        Intent viewIntent =
                new Intent(Intent.ACTION_VIEW, Uri.parse(WEBSITE));
        startActivity(viewIntent);
    }

    @OnClick(R.id.cv_contact_us)
    public void onContactUsClicked() {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", EMAIL, null));
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_hello));
        startActivity(Intent.createChooser(intent, getString(R.string.email_send)));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    public AboutUsFragment() {
    }
}
