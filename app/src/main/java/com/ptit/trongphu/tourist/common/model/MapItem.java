package com.ptit.trongphu.tourist.common.model;

public class MapItem {

    private final String mName;
    private final String mNumber;
    private final String mWebsite;
    private final String mAddress;
    private final Double lat;
    private final Double lon;

    public MapItem(String name, String number, String website, String address, Double lat, Double lon) {
        this.mName = name;
        this.mNumber = number;
        this.mWebsite = website;
        this.mAddress = address;
        this.lat = lat;
        this.lon = lon;
    }

    public String getName() {
        return mName;
    }

    public String getNumber() {
        return mNumber;
    }

    public String getWebsite() {
        return mWebsite;
    }

    public String getAddress() {
        return mAddress;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }
}