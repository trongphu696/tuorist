package com.ptit.trongphu.tourist.views.tourist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.ptit.trongphu.tourist.R;
import com.ptit.trongphu.tourist.common.base.BaseActivity;
import com.ptit.trongphu.tourist.service.GPSTracker;
import com.ptit.trongphu.tourist.utils.TravelmateSnackbars;
import com.ptit.trongphu.tourist.views.tourist.swipefragmentrealtime.ViewPageFragmentsAdapter;

import org.osmdroid.views.MapView;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListViewRealTimeActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    private static final int REQUEST_LOCATION = 199;
    private GPSTracker mTracker;
    private String mCurlat;
    private String mCurlon;
    public ArrayList<Integer> mSelectedIndices = new ArrayList<>();


    @Override
    protected int getLayoutResources() {
        return R.layout.activity_list_real_time;
    }

    @Override
    protected void initVariables(Bundle savedInstanceState) {
        ButterKnife.bind(this);
        setToolbar(toolbar,"List View");
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        mTracker = new GPSTracker(this);
        MapView mMap = findViewById(R.id.action_map_view);
        if (!mTracker.canGetLocation()) {
            mTracker.displayLocationRequest(this, mMap);
        } else {
            //get lat lon  my location
            mCurlat = Double.toString(mTracker.getLatitude());
            mCurlon = Double.toString(mTracker.getLongitude());
            mSelectedIndices.add(0);
        }

        ViewPageFragmentsAdapter adapter = new ViewPageFragmentsAdapter(this, getSupportFragmentManager(), 8,
                mCurlat, mCurlon);
        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);
    }

    private void setToolbar(Toolbar toolbar, String tittle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSupportActionBar(toolbar);
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(tittle);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_back_white);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.list_map_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_map_view:
                finish();
                Intent intent = MapViewRealTimeActivity.getStartIntent(ListViewRealTimeActivity.this);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, ListViewRealTimeActivity.class);
        return intent;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        //User agreed to make required location settings changes
                        //startLocationUpdates();
                        TravelmateSnackbars.createSnackBar(findViewById(R.id.list_view_realtime),
                                R.string.location_enabled, Snackbar.LENGTH_LONG).show();
                        mCurlat = Double.toString(mTracker.getLatitude());
                        mCurlon = Double.toString(mTracker.getLongitude());
                        break;

                    case Activity.RESULT_CANCELED:
                        //User chose not to make required location settings changes
                        TravelmateSnackbars.createSnackBar(findViewById(R.id.list_view_realtime),
                                R.string.location_not_enabled, Snackbar.LENGTH_LONG).show();
                        break;
                }
                break;
        }
    }
}

