package com.ptit.trongphu.tourist.views.mytrips;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.airbnb.lottie.LottieAnimationView;
import com.dd.processbutton.FlatButton;
import com.ptit.trongphu.tourist.R;
import com.ptit.trongphu.tourist.common.base.BaseActivity;
import com.ptit.trongphu.tourist.utils.TravelmateSnackbars;
import com.ptit.trongphu.tourist.utils.Utils;
import com.ptit.trongphu.tourist.views.searchcitydialog.CitySearchDialogCompat;
import com.ptit.trongphu.tourist.views.searchcitydialog.CitySearchModel;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Objects;

import javax.net.ssl.HttpsURLConnection;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.ptit.trongphu.tourist.utils.Constants.BASE_URL;
import static com.ptit.trongphu.tourist.utils.Constants.USER_TOKEN;

public class AddNewTripActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener,
        View.OnClickListener, TravelmateSnackbars {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.select_city_name)
    TextView cityName;
    @BindView(R.id.sdate)
    TextView tripStartDate;
    @BindView(R.id.ok)
    FlatButton ok;
    @BindView(R.id.tname)
    EditText tripName;
    @BindView(R.id.linear_layout)
    LinearLayout mLinearLayout;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;

    private String mCityid;
    private String mStartdate;
    private String mTripname;
    private String mToken;
    private MaterialDialog mDialog;
    private Handler mHandler;
    private DatePickerDialog mDatePickerDialog;
    private ArrayList<CitySearchModel> mSearchCities = new ArrayList<>();

    @Override
    protected int getLayoutResources() {
        return R.layout.activity_add_new_trip;
    }

    @Override
    protected void initVariables(Bundle savedInstanceState) {
        ButterKnife.bind(this);
        setToolbar(toolbar,"Add New Trip");
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mHandler = new Handler(Looper.getMainLooper());
        mToken = sharedPreferences.getString(USER_TOKEN, null);

        final Calendar calendar = Calendar.getInstance();

        mDatePickerDialog = new DatePickerDialog(
                AddNewTripActivity.this,
                AddNewTripActivity.this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));

        fetchCitiesList();

        tripStartDate.setOnClickListener(this);
        ok.setOnClickListener(this);
        cityName.setOnClickListener(this);
    }

    private void setToolbar(Toolbar toolbar, String tittle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSupportActionBar(toolbar);
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(tittle);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_back_white);
        }

    }


    private void addTrip() {
        mDialog = new MaterialDialog.Builder(AddNewTripActivity.this)
                .title(R.string.app_name)
                .content(R.string.progress_wait)
                .progress(true, 0)
                .show();

        String uri = BASE_URL + "add-trip";
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("trip_name", mTripname)
                .addFormDataPart("city_id", mCityid)
                .addFormDataPart("start_date_tx", mStartdate)
                .build();

        Request request = new Request.Builder()
                .header("Authorization", "Token " + mToken)
                .post(requestBody)
                .url(uri)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                mHandler.post(() -> {
                    mDialog.dismiss();
                    networkError();
                });
            }

            @Override
            public void onResponse(Call call, final Response response) {
                try {
                    final String res = Objects.requireNonNull(response.body()).string();
                    final int responseCode = response.code();
                    mHandler.post(() -> {
                        if (responseCode == HttpsURLConnection.HTTP_CREATED) {
                            TravelmateSnackbars.createSnackBar(findViewById(R.id.activityAddNewTrip),
                                    R.string.trip_added, Snackbar.LENGTH_LONG).show();
                            //Call back to MytripsFragment
                            Intent returnIntent = new Intent();
                            setResult(Activity.RESULT_OK , returnIntent);
                            finish();

                        } else {
                            TravelmateSnackbars.createSnackBar(findViewById(R.id.activityAddNewTrip),
                                    res, Snackbar.LENGTH_LONG).show();
                        }
                    });

                } catch (IOException e) {
                    e.printStackTrace();
                    networkError();
                    mDialog.dismiss();
                }
                mDialog.dismiss();

            }
        });
    }

    private void fetchCitiesList() {

        String uri = BASE_URL + "get-all-cities/10";
        Log.v("EXECUTING", uri);

        OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .header("Authorization", "Token " + mToken)
                .url(uri)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("Request Failed", "Message : " + e.getMessage());
            }

            @Override
            public void onResponse(Call call, final Response response) {
                mHandler.post(() -> {
                    if (response.isSuccessful()) {
                        try {
                            assert response.body() != null;
                            String res = response.body().string();
                            Log.v("RESULT", res);
                            JSONArray ar = new JSONArray(res);
                            for (int i = 0; i < ar.length(); i++) {
                                mSearchCities.add(new CitySearchModel(
                                        ar.getJSONObject(i).getString("city_name"),
                                        ar.getJSONObject(i).optString("image"),
                                        ar.getJSONObject(i).getString("id")));
                            }
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                            Log.e("ERROR", "Message : " + e.getMessage());
                        }
                    } else {
                        Log.e("ERROR", "Network error");
                    }
                });
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sdate:
                mDatePickerDialog.show();
                break;
            case R.id.ok:
                Utils.hideKeyboard(this);
                mTripname = tripName.getText().toString();

                if (mTripname.trim().equals("")) {
                    TravelmateSnackbars.createSnackBar(findViewById(R.id.activityAddNewTrip),
                            R.string.trip_name_blank, Snackbar.LENGTH_LONG).show();
                } else if (tripStartDate == null || tripStartDate.getText().toString().equals("")) {
                    TravelmateSnackbars.createSnackBar(findViewById(R.id.activityAddNewTrip),
                            R.string.trip_date_blank, Snackbar.LENGTH_LONG).show();
                } else if (mCityid == null) {
                    TravelmateSnackbars.createSnackBar(findViewById(R.id.activityAddNewTrip),
                            R.string.trip_city_blank, Snackbar.LENGTH_LONG).show();
                } else
                    addTrip();

                break;
            case R.id.select_city_name :
                new CitySearchDialogCompat(AddNewTripActivity.this, getString(R.string.search_title),
                        getString(R.string.search_hint), null, mSearchCities,
                        (SearchResultListener<CitySearchModel>) this::onSelected).show();
                break;

        }
    }

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, AddNewTripActivity.class);
        return intent;
    }


    private void networkError() {
        mLinearLayout.setVisibility(View.INVISIBLE);
        animationView.setVisibility(View.VISIBLE);
        animationView.setAnimation(R.raw.network_lost);
        animationView.playAnimation();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = new GregorianCalendar(year, month, dayOfMonth);
        Log.d("Month", String.valueOf(month));
        mStartdate = Long.toString(calendar.getTimeInMillis() / 1000);
        tripStartDate.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
    }

    private void onSelected(BaseSearchDialogCompat dialog, CitySearchModel item, int position) {
        mCityid = item.getId();
        cityName.setText(item.getTitle());
        dialog.dismiss();
    }
}
