package com.ptit.trongphu.tourist.views.destinations.funfacts;

import com.ptit.trongphu.tourist.common.model.FunFact;

import java.util.ArrayList;



interface FunFactsView {
    void showProgressDialog();
    void hideProgressDialog();
    void setupViewPager(ArrayList<FunFact> factsArray);
}
