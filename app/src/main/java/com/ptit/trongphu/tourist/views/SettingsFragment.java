package com.ptit.trongphu.tourist.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.dd.processbutton.iml.ActionProcessButton;
import com.ptit.trongphu.tourist.R;
import com.ptit.trongphu.tourist.common.base.BaseFragment;
import com.ptit.trongphu.tourist.utils.TravelmateSnackbars;
import com.ptit.trongphu.tourist.utils.Utils;
import com.ptit.trongphu.tourist.views.login.LoginActivity;

import java.io.IOException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.ptit.trongphu.tourist.utils.Constants.BASE_URL;
import static com.ptit.trongphu.tourist.utils.Constants.USER_TOKEN;


public class SettingsFragment extends BaseFragment {

    @BindView(R.id.old_password)
    EditText oldPasswordText;
    @BindView(R.id.new_password)
    EditText newPasswordText;
    @BindView(R.id.connfirm_password)
    EditText confirmPasswordText;
    @BindView(R.id.done_button)
    ActionProcessButton doneButton;
    @BindView(R.id.delete_button)
    ActionProcessButton deleteButton;
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;

    private String mToken;
    private Handler mHandler;
    private Activity mActivity;
    private SharedPreferences mSharedPreferences;

    public SettingsFragment() {
    }

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity) context;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_settings;
    }

    @Override
    protected void initVariables(Bundle saveInstanceState, View rootView) {
        ButterKnife.bind(this, rootView);
    }

    @Override
    protected void initData(Bundle saveInstanceState) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mActivity);
        mToken = mSharedPreferences.getString(USER_TOKEN, null);
        mHandler = new Handler(Looper.getMainLooper());

        doneButton.setMode(ActionProcessButton.Mode.ENDLESS);
        doneButton.setOnClickListener(v -> {
            Utils.hideKeyboard(Objects.requireNonNull(getActivity()));
            if (checkEmptyText())
                checkPasswordMatch();
        });

        deleteButton.setOnClickListener(v -> deleteAccount());
    }


    private boolean checkEmptyText() {
        if (oldPasswordText.getText().toString().equals("")) {
            oldPasswordText.setError(getString(R.string.required_error));
            oldPasswordText.requestFocus();
            return false;
        } else if (newPasswordText.getText().toString().equals("")) {
            newPasswordText.setError(getString(R.string.required_error));
            newPasswordText.requestFocus();
            return false;
        } else if (confirmPasswordText.getText().toString().equals("")) {
            confirmPasswordText.setError(getString(R.string.required_error));
            confirmPasswordText.requestFocus();
            return false;
        } else {
            return true;
        }

    }

    private void checkPasswordMatch() {
        String oldPassword = oldPasswordText.getText().toString();
        String newPassword = newPasswordText.getText().toString();
        String confirmPassword = confirmPasswordText.getText().toString();
        if (validatePassword(newPassword)) {
            if (newPassword.equals(confirmPassword)) {
                changePassword(newPassword, oldPassword);
            } else {
                Snackbar snackbar = Snackbar
                        .make(mActivity.findViewById(android.R.id.content),
                                R.string.passwords_check, Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    public boolean validatePassword(String passString) {
        if (passString.length() >= 8) {
            Pattern pattern;
            Matcher matcher;
            final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
            pattern = Pattern.compile(PASSWORD_PATTERN);
            matcher = pattern.matcher(passString);

            if (matcher.matches()) {
                return true;
            } else {
                Snackbar snackbar = Snackbar
                        .make(mActivity.findViewById(android.R.id.content),
                                R.string.invalid_password, Snackbar.LENGTH_LONG);
                snackbar.show();
                return false;
            }
        } else {
            Snackbar snackbar = Snackbar
                    .make(mActivity.findViewById(android.R.id.content),
                            R.string.password_length, Snackbar.LENGTH_LONG);
            snackbar.show();
            return false;
        }
    }

    public void changePassword(String newPassword, String oldPassword) {

        doneButton.setProgress(1);
        String uri = BASE_URL + "update-password";
        Log.v("EXECUTING", uri);
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("old_password", oldPassword)
                .addFormDataPart("new_password", newPassword)
                .build();

        Request request = new Request.Builder()
                .header("Authorization", "Token " + mToken)
                .url(uri)
                .post(requestBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("Request Failed", "Message : " + e.getMessage());
                mHandler.post(() -> networkError());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String res = Objects.requireNonNull(response.body()).string();
                doneButton.setProgress(0);
                mHandler.post(() -> {
                    if (response.isSuccessful()) {
                        clearText();
                        TravelmateSnackbars.createSnackBar(mActivity.findViewById(android.R.id.content)
                                , res, Snackbar.LENGTH_LONG).show();
                    } else {
                        TravelmateSnackbars.createSnackBar(mActivity.findViewById(android.R.id.content)
                                , res, Snackbar.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    private void deleteAccount() {
        ContextThemeWrapper crt = new ContextThemeWrapper(mActivity, R.style.AlertDialog);
        AlertDialog.Builder builder = new AlertDialog.Builder(crt);
        builder.setMessage(R.string.delete_account)
                .setPositiveButton(R.string.positive_button,
                        (dialog, which) -> {

                            String uri = BASE_URL + "delete-profile";
                            //Set up client
                            OkHttpClient client = new OkHttpClient();

                            Request request = new Request.Builder()
                                    .header("Authorization", "Token " + mToken)
                                    .url(uri)
                                    .build();

                            client.newCall(request).enqueue(new Callback() {
                                @Override
                                public void onFailure(Call call, IOException e) {
                                    Log.e("Request Failed", "Message : " + e.getMessage());
                                    mHandler.post(() -> networkError());
                                }

                                @Override
                                public void onResponse(Call call, Response response){
                                    mHandler.post(() -> {
                                        if (response.isSuccessful()) {
                                            mSharedPreferences
                                                    .edit()
                                                    .putString(USER_TOKEN, null)
                                                    .apply();

                                            Intent intent = LoginActivity.getStartIntent(mActivity);
                                            mActivity.startActivity(intent);
                                            mActivity.finish();
                                        }
                                    });

                                }
                            });

                        })
                .setNegativeButton(android.R.string.cancel,
                        (dialog, which) -> {

                        });
        builder.create().show();
    }
    private void networkError() {
        layout.setVisibility(View.INVISIBLE);
        animationView.setVisibility(View.VISIBLE);
        animationView.setAnimation(R.raw.network_lost);
        animationView.playAnimation();
    }

       private void clearText() {
        oldPasswordText.setText("");
        newPasswordText.setText("");
        confirmPasswordText.setText("");
    }

}
