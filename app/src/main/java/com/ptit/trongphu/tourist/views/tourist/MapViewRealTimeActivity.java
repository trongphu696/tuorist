package com.ptit.trongphu.tourist.views.tourist;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.airbnb.lottie.LottieAnimationView;
import com.ptit.trongphu.tourist.R;
import com.ptit.trongphu.tourist.common.base.BaseActivity;
import com.ptit.trongphu.tourist.common.model.MapItem;
import com.ptit.trongphu.tourist.service.GPSTracker;
import com.ptit.trongphu.tourist.utils.TravelmateSnackbars;

import org.json.JSONArray;
import org.json.JSONException;
import org.osmdroid.api.IMapController;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.ptit.trongphu.tourist.utils.Constants.BASE_URL;
import static com.ptit.trongphu.tourist.utils.Constants.HERE_API_MODES;
import static com.ptit.trongphu.tourist.utils.Constants.USER_TOKEN;

public class MapViewRealTimeActivity extends BaseActivity implements
        TravelmateSnackbars, Marker.OnMarkerClickListener {

    private final List<MapItem> mMapItems = new ArrayList<>();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.data)
    ScrollView scrollView;
    @BindView(R.id.animation)
    LottieAnimationView animationView;
    @BindView(R.id.layout)
    LinearLayout layout;
    private int mIndex = 0;
    private Handler mHandler;
    private String mToken;
    private String mCurlat;
    private String mCurlon;
    private MapView mMap;
    private IMapController mController;
    private static final int REQUEST_LOCATION = 199;
    GPSTracker tracker;
    public ArrayList<Integer> mSelectedIndices = new ArrayList<>();

    @Override
    protected int getLayoutResources() {
        return R.layout.activity_map_realtime;
    }

    @Override
    protected void initVariables(Bundle savedInstanceState) {
        ButterKnife.bind(this);
        setToolbar(toolbar);
    }

    private void setToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Map View");
        }
    }


    @Override
    protected void initData(Bundle savedInstanceState) {
        mHandler = new Handler(Looper.getMainLooper());
        mMap = findViewById(R.id.map);
        scrollView.setVisibility(View.GONE);
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mToken = mSharedPreferences.getString(USER_TOKEN, null);

        initMap();


        // Get user's current location
        tracker = new GPSTracker(this);
        if (!tracker.canGetLocation()) {
            tracker.displayLocationRequest(this, mMap);
        } else {
            mCurlat = Double.toString(tracker.getLatitude());
            mCurlon = Double.toString(tracker.getLongitude());
            showDialogToSelectitems();
        }
    }


    private void initMap() {
        mMap.setBuiltInZoomControls(false);
        mMap.setMultiTouchControls(true);
        mMap.setTilesScaledToDpi(true);
        mController = mMap.getController();


        MyLocationNewOverlay mLocationoverlay = new MyLocationNewOverlay(mMap);
        mLocationoverlay.enableFollowLocation();
        mLocationoverlay.enableMyLocation();
        mMap.getOverlays().add(mLocationoverlay);
        mController.setZoom(14);
    }

    private void getMarkers(String mode, final int icon) {
        String uri = BASE_URL + "get-places/" + mCurlat + "/" + mCurlon + "/" + mode;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .header("Authorization", "Token " + mToken)
                .url(uri)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("Request Failed", "Message : " + e.getMessage());
                mHandler.post(() -> networkError());
            }

            @Override
            public void onResponse(final Call call, final Response response) throws IOException {

                final String res = Objects.requireNonNull(response.body()).string();
                mHandler.post(() -> {
                    try {
                        JSONArray routeArray = new JSONArray(res);

                        for (int i = 0; i < routeArray.length(); i++) {
                            String name = routeArray.getJSONObject(i).getString("title");
                            String web = routeArray.getJSONObject(i).getString("icon");
                            String nums = routeArray.getJSONObject(i).getString("distance");
                            String addr = routeArray.getJSONObject(i).getString("address");

                            Double lat =
                                    routeArray.getJSONObject(i).getDouble("latitude");
                            Double lon =
                                    routeArray.getJSONObject(i).getDouble("longitude");

                            showMarker(lat, lon, name, icon);
                            mMapItems.add(new MapItem(name, nums, web, addr, lat, lon));
                        }
                        animationView.setVisibility(View.GONE);
                        layout.setVisibility(View.VISIBLE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        networkError();
                        Log.e("ERROR : ", e.getMessage() + " ");
                    }
                });
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_sort:
                showDialogToSelectitems();
                return true;
            case R.id.action_list_view:
                finish();
                Intent intent = ListViewRealTimeActivity.getStartIntent(MapViewRealTimeActivity.this);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void showDialogToSelectitems() {
        Integer[] selectedItems = new Integer[mSelectedIndices.size()];

        for (int i = 0; i < mSelectedIndices.size(); i++) {
            selectedItems[i] = mSelectedIndices.get(i);
        }
        mSelectedIndices.clear();
        new MaterialDialog.Builder(this)
                .title(R.string.title)
                .items(R.array.items)
                .itemsCallbackMultiChoice(selectedItems, (dialog, which, text) -> {

                    //    mGoogleMap.clear();
                    mMapItems.clear();

                    for (int i = 0; i < which.length; i++) {
                        Log.v("selected", which[i] + " " + text[i]);
                        mSelectedIndices.add(which[i]);
                        Integer icon;
                        switch (which[i]) {
                            case 0:
                                icon = R.drawable.ic_local_pizza_black;
                                break;
                            case 1:
                                icon = R.drawable.ic_local_bar_black;
                                break;
                            case 2:
                                icon = R.drawable.ic_camera_alt_black;
                                break;
                            case 3:
                                icon = R.drawable.ic_directions_bus_black;
                                break;
                            case 4:
                                icon = R.drawable.ic_local_mall_black;
                                break;
                            case 5:
                                icon = R.drawable.ic_local_gas_station_black;
                                break;
                            case 6:
                                icon = R.drawable.ic_local_atm_black;
                                break;
                            case 7:
                                icon = R.drawable.ic_local_hospital_black;
                                break;
                            default:
                                icon = R.drawable.ic_attach_money_black;
                                break;
                        }
                        MapViewRealTimeActivity.this.getMarkers(HERE_API_MODES.get(which[i]), icon);
                    }
                    return true;
                })
                .positiveText(R.string.choose)
                .show();
    }


    private void showMarker(Double locationLat, Double locationLong, String locationName, Integer locationIcon) {
        GeoPoint coord = new GeoPoint(locationLat, locationLong);
        Marker marker = new Marker(mMap);

        if (ContextCompat.checkSelfPermission(MapViewRealTimeActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            marker.setPosition(coord);
            marker.setIcon(this.getResources().getDrawable(locationIcon));
            marker.setTitle(locationName);
            marker.setOnMarkerClickListener(this);

            mMap.getOverlays().add(marker);
            mMap.invalidate();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map_menu, menu);
        return true;
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context, MapViewRealTimeActivity.class);
    }


    private void onMarkerSelected(Marker marker) {
        // Zoom to current location
        GeoPoint coordinate = new GeoPoint(Double.parseDouble(mCurlat), Double.parseDouble(mCurlon));
        mController.setZoom(14);
        mController.animateTo(coordinate);

        marker.showInfoWindow();

        scrollView.setVisibility(View.VISIBLE);
        for (int i = 0; i < mMapItems.size(); i++) {
            if (mMapItems.get(i).getName().equals(marker.getTitle())) {
                mIndex = i;
                break;
            }
        }

        TextView title = MapViewRealTimeActivity.this.findViewById(R.id.item_title);
        TextView description = MapViewRealTimeActivity.this.findViewById(R.id.item_description);
        final Button calls, book;
        calls = MapViewRealTimeActivity.this.findViewById(R.id.call);
        book = MapViewRealTimeActivity.this.findViewById(R.id.book);

        title.setText(mMapItems.get(mIndex).getName());

        description.setText(android.text.Html.fromHtml(mMapItems.get(mIndex).getAddress()).toString());
        calls.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + mMapItems.get(mIndex).getNumber()));
            MapViewRealTimeActivity.this.startActivity(intent);

        });
        book.setOnClickListener(view -> {
            Intent browserIntent;
            try {
                browserIntent = new Intent(
                        Intent.ACTION_VIEW, Uri.parse(mMapItems.get(mIndex).getAddress()));
                MapViewRealTimeActivity.this.startActivity(browserIntent);
            } catch (Exception e) {
                TravelmateSnackbars.createSnackBar(findViewById(R.id.map_real_time),
                        R.string.no_activity_for_browser, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK:

                        TravelmateSnackbars.createSnackBar(findViewById(R.id.map_real_time),
                                R.string.location_enabled, Snackbar.LENGTH_LONG).show();
                        mCurlat = Double.toString(tracker.getLatitude());
                        mCurlon = Double.toString(tracker.getLongitude());
                        getMarkers("eat-drink", R.drawable.ic_local_pizza_black);

                        break;
                    case Activity.RESULT_CANCELED:
                        TravelmateSnackbars.createSnackBar(findViewById(R.id.map_real_time),
                                R.string.location_not_enabled, Snackbar.LENGTH_LONG).show();
                        break;
                }
                break;
        }
    }


    @Override
    public boolean onMarkerClick(Marker marker, MapView mapView) {
        onMarkerSelected(marker);
        return false;
    }


    private void networkError() {
        layout.setVisibility(View.GONE);
        animationView.setVisibility(View.VISIBLE);
        animationView.setAnimation(R.raw.network_lost);
        animationView.playAnimation();
    }
}
