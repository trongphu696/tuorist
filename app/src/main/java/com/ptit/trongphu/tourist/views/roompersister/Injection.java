package com.ptit.trongphu.tourist.views.roompersister;

import android.content.Context;

public class Injection {

    public static ChecklistDataSource provideUserDataSource(Context context) {
        DbChecklist database = DbChecklist.getsInstance(context);
        return new ChecklistDataSource(database.checklistItemDAO());
    }

    public static ViewModelFactory provideViewModelFactory(Context context) {
        ChecklistDataSource dataSource = provideUserDataSource(context);
        return new ViewModelFactory(dataSource);
    }
}
