package com.ptit.trongphu.tourist.views.destinations.description;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.ptit.trongphu.tourist.R;
import com.ptit.trongphu.tourist.common.base.BaseActivity;
import com.ptit.trongphu.tourist.common.model.City;
import com.ptit.trongphu.tourist.common.model.Weather;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.ptit.trongphu.tourist.utils.Constants.BASE_URL;
import static com.ptit.trongphu.tourist.utils.Constants.CURRENT_TEMP;
import static com.ptit.trongphu.tourist.utils.Constants.EXTRA_MESSAGE_CALLED_FROM_UTILITIES;
import static com.ptit.trongphu.tourist.utils.Constants.EXTRA_MESSAGE_CITY_ID;
import static com.ptit.trongphu.tourist.utils.Constants.EXTRA_MESSAGE_CITY_NAME;
import static com.ptit.trongphu.tourist.utils.Constants.EXTRA_MESSAGE_CITY_OBJECT;
import static com.ptit.trongphu.tourist.utils.Constants.NUM_DAYS;
import static com.ptit.trongphu.tourist.utils.Constants.USER_TOKEN;
import static com.ptit.trongphu.tourist.utils.WeatherUtils.fetchDrawableFileResource;
import static com.ptit.trongphu.tourist.utils.WeatherUtils.getDayOfWeek;

public class WeatherActivity extends BaseActivity {

    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    @BindView(R.id.weather_condition)
    TextView condition;
    @BindView(R.id.weather_icon)
    ImageView icon;
    @BindView(R.id.temp)
    TextView temp;
    @BindView(R.id.min_temp)
    TextView maxTemp;
    @BindView(R.id.max_temp)
    TextView minTemp;
    @BindView(R.id.today)
    TextView today;
    @BindView(R.id.day_of_week)
    TextView dayOfweek;
    @BindView(R.id.forecast_list)
    RecyclerView forecastList;
    @BindView(R.id.empty_textview)
    TextView emptyView;

    private City mCity;
    private String mToken;
    private String mCurrentTemp;
    private Handler mHandler;
    private ArrayList<Weather> mWeatherList = new ArrayList<>();

    @Override
    protected int getLayoutResources() {
        return R.layout.activity_weather;
    }

    @Override
    protected void initVariables(Bundle savedInstanceState) {
        ButterKnife.bind(this);
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mToken = sharedPreferences.getString(USER_TOKEN, null);

        mHandler = new Handler(Looper.getMainLooper());

        Intent intent = getIntent();

        boolean isCalledFromUtilities = intent.getBooleanExtra(EXTRA_MESSAGE_CALLED_FROM_UTILITIES,
                false);
        if (isCalledFromUtilities) {
            String cityNickname = intent.getStringExtra(EXTRA_MESSAGE_CITY_NAME);
            String cityID = intent.getStringExtra(EXTRA_MESSAGE_CITY_ID);
            mCity = new City(cityNickname, cityID);
            fetchCurrentTemp();
        } else {
            mCity = (City) intent.getSerializableExtra(EXTRA_MESSAGE_CITY_OBJECT);
            mCurrentTemp = intent.getStringExtra(CURRENT_TEMP);
            if (mCurrentTemp != null) {
                fetchWeatherForecast();
            } else {
                fetchCurrentTemp();
            }

        }
        emptyView.setText(String.format(getString(R.string.city_not_found), mCity.getNickname()));

    }


    private void fetchWeatherForecast() {
        String uri = BASE_URL + "get-multiple-days-weather/" + NUM_DAYS + "/" + mCity.getNickname();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .header("Authorization", "Token " + mToken)
                .url(uri)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                mHandler.post(() -> {
                    Log.e("Request Failed", "Message : " + e.getMessage());
                    cancelAnimation();
                    networkError();
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String jsonResponse = Objects.requireNonNull(response.body()).string();
                mHandler.post(() -> {
                    if (response.isSuccessful()) {
                        try {
                            JSONArray array = new JSONArray(jsonResponse);
                            for (int i = 0; i < array.length(); i++) {

                                String weatherCondition = array.getJSONObject(i).getString("condensed");

                                double maxT = array.getJSONObject(i).getDouble("max_temp");
                                double minT = array.getJSONObject(i).getDouble("min_temp");
                                int maxTemperature = (int) Math.rint(maxT);
                                int minTemperature = (int) Math.rint(minT);

                                String dayOfWeek = getDayOfWeek(i, "EEEE");

                                String iconUrl = array.getJSONObject(i).getString("icon");
                                int code = array.getJSONObject(i).getInt("code");
                                int id = fetchDrawableFileResource(WeatherActivity.this, iconUrl, code);
                                if (i == 0) {
                                    condition.setText(weatherCondition);
                                    temp.setText(mCurrentTemp);
                                    maxTemp.setText(String.valueOf(maxTemperature));
                                    maxTemp.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arrow_upward,
                                            0, 0, 0);
                                    minTemp.setText(String.valueOf(minTemperature));
                                    minTemp.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arrow_downward,
                                            0, 0, 0);
                                    dayOfweek.setText(dayOfWeek);
                                    icon.setImageResource(id);
                                    DrawableCompat.setTint(icon.getDrawable(),
                                            ContextCompat.getColor(WeatherActivity.this, android.R.color.white));
                                    today.setText(R.string.today);

                                } else {
                                    String day = getDayOfWeek(i, "dd MMM");
                                    Weather weather = new Weather(id, maxTemperature,
                                            minTemperature, dayOfWeek.substring(0, 3), day);
                                    mWeatherList.add(weather);

                                    forecastList.addItemDecoration(new DividerItemDecoration(WeatherActivity.this,
                                            DividerItemDecoration.HORIZONTAL));
                                    forecastList.setLayoutManager(new LinearLayoutManager(WeatherActivity.this,
                                            LinearLayoutManager.HORIZONTAL, false));

                                    WeatherAdapter adapter = new WeatherAdapter(WeatherActivity.this, mWeatherList);
                                    forecastList.setAdapter(adapter);
                                }
                            }
                            cancelAnimation();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            emptyListAnimation();
                        } catch (IOException e) {
                            networkError();
                        }
                    } else {
                        emptyListAnimation();
                    }
                });
            }

        });
    }


    private void fetchCurrentTemp() {

        String uri = BASE_URL + "get-city-weather/" + mCity.getId();
        uri = uri.replaceAll(" ", "%20");
        OkHttpClient client = new OkHttpClient();
        Log.v("EXECUTING", uri);
        Request request = new Request.Builder()
                .header("Authorization", "Token " + mToken)
                .url(uri)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                mHandler.post(() -> networkError());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful() && response.body() != null) {

                    final String res = Objects.requireNonNull(response.body()).string();
                    try {
                        JSONObject responseObject = new JSONObject(res);
                        mCurrentTemp = responseObject.getString("temp") +
                                (char) 0x00B0 + responseObject.getString("temp_units");
                        fetchWeatherForecast();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        mHandler.post(() -> networkError());
                    }
                } else {
                    mHandler.post(() -> emptyListAnimation());
                }
            }
        });
    }

    private void setToolbar(Toolbar toolbar, String tittle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSupportActionBar(toolbar);
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(tittle);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_back_white);
        }

    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    public static Intent getStartIntent(Context context, City city, String currentTemp) {
        Intent intent = new Intent(context, WeatherActivity.class);
        intent.putExtra(EXTRA_MESSAGE_CITY_OBJECT, city);
        intent.putExtra(CURRENT_TEMP, currentTemp);
        return intent;
    }

    public static Intent getStartIntent(Context context, String cityName,
                                        String cityId, boolean calledFromUtilities) {
        Intent intent = new Intent(context, WeatherActivity.class);
        intent.putExtra(EXTRA_MESSAGE_CITY_NAME, cityName);
        intent.putExtra(EXTRA_MESSAGE_CITY_ID, cityId);
        intent.putExtra(EXTRA_MESSAGE_CALLED_FROM_UTILITIES, calledFromUtilities);
        return intent;
    }


    private void networkError() {
        animationView.setVisibility(View.VISIBLE);
        animationView.setAnimation(R.raw.network_lost);
        animationView.playAnimation();
    }

    private void emptyListAnimation() {
        emptyView.setVisibility(View.VISIBLE);
        animationView.setVisibility(View.VISIBLE);
        animationView.setAnimation(R.raw.empty_list);
        animationView.playAnimation();
    }


    private void cancelAnimation() {
        if (animationView != null) {
            animationView.cancelAnimation();
            animationView.setVisibility(View.GONE);
        }
    }
}
