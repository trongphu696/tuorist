package com.ptit.trongphu.tourist.views.roompersister;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;

import com.ptit.trongphu.tourist.common.model.ChecklistItem;

@Database(entities = ChecklistItem.class, version = 5, exportSchema = false)
public abstract class DbChecklist extends RoomDatabase {

    private static final Object LOCK = new Object();
    private static final String DATABASE_NAME = "Tourist.db";
    private static DbChecklist sInstance;

    public static DbChecklist getsInstance(Context context) {
        //sigleton patern
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = Room.databaseBuilder(context.getApplicationContext(),
                        DbChecklist.class, DbChecklist.DATABASE_NAME)
                        .addMigrations(MIGRATION_3_4, MIGRATION_4_5)
                        .build();
            }
        }
        return sInstance;
    }


    static final Migration MIGRATION_3_4 = new Migration(3, 4) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL(
                    "CREATE TABLE checklist_items (id INTEGER PRIMARY KEY NOT NULL, name TEXT," +
                            " isDone TEXT)");
            database.execSQL(
                    "INSERT INTO checklist_items (id, name, isDone) " +
                            "SELECT id, name, isDone FROM events_new");
            database.execSQL("DROP TABLE events_new");
            database.execSQL("ALTER TABLE checklist_items RENAME TO events_new");
        }
    };

    //migration from database version 4 to 5
    static final Migration MIGRATION_4_5 = new Migration(4, 5) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL(
                    "CREATE TABLE checklist_items (id INTEGER PRIMARY KEY NOT NULL, name TEXT," +
                            " isDone TEXT NOT NULL, position INTEGER DEFAULT 0 NOT NULL)");

            database.execSQL("CREATE TABLE seq_generator(pos INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "id INTEGER)");
            database.execSQL("INSERT INTO seq_generator (id)" + "SELECT id from events_new");
            database.execSQL(
                    "INSERT INTO checklist_items " +
                            "SELECT old.id, old.name, old.isDone, t.pos-1 " +
                            "FROM events_new old JOIN seq_generator t ON old.id = t.id");

            database.execSQL("DROP TABLE seq_generator");
            database.execSQL("DROP TABLE events_new");
            database.execSQL("ALTER TABLE checklist_items RENAME TO events_new");
        }
    };

    public abstract ChecklistItemDAO checklistItemDAO();

}
