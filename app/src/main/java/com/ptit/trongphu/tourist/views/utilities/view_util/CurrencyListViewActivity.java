package com.ptit.trongphu.tourist.views.utilities.view_util;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.ptit.trongphu.tourist.R;
import com.ptit.trongphu.tourist.common.base.BaseActivity;
import com.ptit.trongphu.tourist.common.model.ZoneName;
import com.ptit.trongphu.tourist.common.adapter.CurrencyConverterAdapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CurrencyListViewActivity extends BaseActivity implements TextWatcher {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.listView)
    RecyclerView mListview;
    @BindView(R.id.currencySearch)
    EditText mCurrencySearch;
    CurrencyConverterAdapter mAdaptorListView;
    String temp = null;
    public static ArrayList<ZoneName> currences_names;
    String s_ids_names;
    private Context mContext;
    StringTokenizer stok;

    @Override
    protected int getLayoutResources() {
        return R.layout.activity_conversion_listview;
    }

    @Override
    protected void initVariables(Bundle savedInstanceState) {
        ButterKnife.bind(this);
        setToolbar(toolbar,"List Country Currency");
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        currences_names = new ArrayList<>();
        mListview = findViewById(R.id.listView);
        mCurrencySearch = findViewById(R.id.currencySearch);

        mContext = this;
        addCurrencies();

        mCurrencySearch.addTextChangedListener(this);
    }

    private void setToolbar(Toolbar toolbar, String tittle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSupportActionBar(toolbar);
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(tittle);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_back_white);
        }
    }

    public void addCurrencies() {
        try {
            InputStream is = mContext.getAssets().open("currencies.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            s_ids_names = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        s_ids_names = s_ids_names.replace("{", "");
        s_ids_names = s_ids_names.replace("}", "");
        s_ids_names = s_ids_names.replace("\"", "");
        stok = new StringTokenizer(s_ids_names, ",");

        while (stok.hasMoreElements()) {
            temp = stok.nextElement().toString();

            if (temp.contains("currencySymbol")) {
                temp = stok.nextElement().toString();
            }

            String[] split = temp.split(":");
            temp = stok.nextElement().toString();
            if (temp.contains("currencySymbol")) {
                temp = stok.nextElement().toString();
            }
            String[] split2 = temp.split(":");
            temp = null;
            currences_names.add(new ZoneName(split[2], split2[1]));
        }

        Collections.sort(currences_names, (n1, n2) -> n1.shortName.compareTo(n2.shortName));

        mAdaptorListView = new CurrencyConverterAdapter(CurrencyListViewActivity.this, currences_names);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext.getApplicationContext());
        mListview.setLayoutManager(mLayoutManager);
        mListview.setAdapter(mAdaptorListView);
    }

    @Override
    public void beforeTextChanged(CharSequence searchItem, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence searchItem, int start, int before, int count) {
        CurrencyListViewActivity.this.mAdaptorListView.getFilter().filter(searchItem);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}


